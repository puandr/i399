'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID;
const Dao = require('./dao.js');
const mongoUrl = 'mongodb://andrei:andrei@ds121889.mlab.com:21889/i399';

const dao = new Dao();
const app = express();

app.use(bodyParser.json()); // before request handlers
app.use(express.static('./'));

app.get('/api/contacts', getContacts);
app.get('/api/contacts/:id', getContact);
app.post('/api/contacts', addContact);
app.put('/api/contacts/:id', changeContact);
app.post('/api/contacts/delete', deleteMultipleContacts);
app.delete('/api/contacts/:id', deleteContact);

app.use(errorHandler); // after request handlers

dao.connect(mongoUrl)
    .then(() => {
        app.listen(3000);
        console.log('Server is running...')})
    .catch(error => {
        console.log(error);
        process.exit(1);
});


function getContacts(request, response, next) {
    dao.findAll()
        .then(data => response.json(data))
        .catch(next);
}


function getContact(request, response, next) {
    let id = request.params.id;

    dao.findById(id)
        .then(data => response.json(data))
        .catch(next)
}


function addContact(request, response, next) {
    let contact = request.body;
    let tempContact = {
        "name": contact.name,
        "phone": contact.phone
    };

    dao.insert(tempContact)
        .then((data) => {
            response.location("/api/contacts/" + data.insertedId);
            dao.findById(data.insertedId)
                .then(insertedContact => response.json(insertedContact))
        })
        .catch(next)
}


function changeContact(request, response, next) {
    let id = request.params.id;
    let contact = request.body;
    let tempContact = {
        "name": contact.name,
        "phone": contact.phone
    };

    dao.update(id, tempContact)
        .then(data => response.end())
        .catch(next);
}


function deleteContact(request, response, next) {
    let id = request.params.id;

    dao.remove(id)
        .then(data => response.end())
        .catch(next)
}


function deleteMultipleContacts(request, response, next) {
    let contactIds = request.body;

    dao.removeMany(contactIds)
        .then(data => response.end())
        .catch(next)
}


function errorHandler(error, request, response, next) { // here must be 4 arguments, the way servers knows its an error handler
    console.log(error);
    response.status(500).send('error: ' + error.toString());
    response.status(400).send('error: ' + error.toString());
    response.status(404).send('error: ' + error.toString());
}
