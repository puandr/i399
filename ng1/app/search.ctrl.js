(function (){
    angular.module('app').controller('searchCtrl', Ctrl);

    function Ctrl ($http, $routeParams, modalService){
        //var contacts = [];
        var vm = this;
        this.removeContact = removeContact;

        initContactsDisplay();
        
        function initContactsDisplay() {
            $http.get('api/contacts').then(function(result){
                vm.contacts = result.data;
            })
        }

        function removeContact(id) {
            modalService.confirm().then(function () {
                return $http.delete('api/contacts/' + id);
            }).then(initContactsDisplay);
        }

    }
})();