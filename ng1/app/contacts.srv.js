(function () {
    angular.module('app').service('contactsService', Srv);

    function Srv($http) {
        let srv = this;
        srv.getContacts = getContacts;
        srv.getContact = getContact;
        srv.addContact = addContact;
        srv.editContact = editContact;
        srv.deleteContact = deleteContact;

        function getContacts() {
            return $http.get("/api/contacts")
                .then(function(resp){
                    return resp.data})
                .catch(function(e) {
                    console.error("Unable to get contacts list", e)
                })
        }


        function getContact(id) {
            return $http.get("/api/contacts/" + id)
                .then(function(resp) {
                    return resp.data})
                .catch(function(e) {
                    console.error("Unable to get contact by Id: " + id, e)
                })
        }


        function addContact(contact) {
            return $http.post("/api/contacts", contact)
                .then(function(resp){
                    return resp.data})
                .catch(function(e) {
                    console.error("Unable to add new contact", e)
                })
        }


        function editContact(id, contact) {
            return $http.put("/api/contacts/" + id, contact)
                .then(function(resp){
                    return resp.data})
                .catch(function(e) {
                    console.error("Unable to modify contact with id: " + id, e)
                })
        }


        function deleteContact(id) {
            return $http.delete("/api/contacts/" + id)
                .then(function(resp) {
                    return resp.data;})
                .catch(function(e) {
                    console.error("Unable to delete contact by id: " + id, e)
                })
        }
    }
})();
