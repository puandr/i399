(function () {
    'use strict';

    angular.module('app').config(RouteConfig);

    function RouteConfig($routeProvider) {
        //console.log('im in routes.js');
        $routeProvider.when('/search', {
            templateUrl : 'app/search.html',
            controller : 'searchCtrl',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : 'app/new.html',
            controller : 'newCtrl',
            controllerAs : 'vm'
        }).when('/edit/:id', {
            templateUrl : 'app/edit.html',
            controller : 'editCtrl',
            controllerAs : 'vm'
        }).otherwise('/search');
    }
})();