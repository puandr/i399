(function () {
    angular.module('app').controller('editCtrl', Ctrl);
    
    function Ctrl($http, $routeParams, $location) {
        var vm = this;
        this.changeContact = changeContact;

        vm.contact = {};

        $http.get('api/contacts/' + $routeParams.id).then(function (result) {
            vm.contact = result.data;
        });

        function changeContact() {
            var newContact = {
                _id : vm.contact._id,
                name : vm.contact.name,
                phone : vm.contact.phone
            };
            //console.log('im in changeContact');
            $http.put('api/contacts/' + $routeParams.id, newContact).then($location.path('/#search'));
        }


        
    }

}());