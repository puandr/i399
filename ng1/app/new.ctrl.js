(function (){
    angular.module('app').controller('newCtrl', Ctrl);

    function Ctrl ($http, $location){
        var vm = this;
        this.addNew = addNew;

        function addNew() {
            var newContact = {
                name: this.contactName,
                phone:  this.contactPhone
            };
            $http.post('api/contacts', newContact).then($location.path('/#search'));

        }
    }
})();