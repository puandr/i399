import { Component, OnInit } from '@angular/core';
import {Contact} from "../contact";
import {ContactService} from "../contact.srv";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-edit',
  templateUrl: `edit.html`,
  styles: []
})
export class EditComponent implements OnInit {

  constructor(private contactService: ContactService, private router: Router, private activatedRoute: ActivatedRoute ) { }

    contact: Contact;
    id: number;
    tempContactName: string;
    tempContactPhone: string;

    editContact(){
        this.contact.name = this.tempContactName;
        this.contact.phone = this.tempContactPhone;
        this.contactService.editContact(this.id, this.contact)
            .then(() => this.router.navigateByUrl('/search'));;
    }

  ngOnInit(): void {
      this.id = this.activatedRoute.snapshot.params['id'];
      this.contactService.getContact(this.id).then((contact) => {
          this.contact = contact;
          this.tempContactName = contact.name;
          this.tempContactPhone = contact.phone;
          return;
      });
  }

}
