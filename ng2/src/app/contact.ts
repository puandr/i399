export class Contact {
    _id: number;
    checked: boolean = false;

    constructor(public name: string, public phone: string){    }
}
