import { Component, OnInit } from '@angular/core';
import {Contact} from "../contact";
import {ContactService} from "../contact.srv";
import { FilterPipe } from '../pipe';

@Component({
  selector: 'app-search',
  templateUrl: 'search.html',
  styles: []
})
export class SearchComponent implements OnInit {

  constructor(private contactService: ContactService) { }
  contacts: Contact[] = [];
  queryString: string;
  query: string = this.queryString;
  selectedContacts : Contact[];
  selectedId: number[];


  initContactsDisplay() {
      this.contactService.getContacts()
          .then(contacts => this.contacts = contacts);
  }


  removeContact(id: number): void {
    this.contactService.deleteContact(id)
        .then(() => this.initContactsDisplay());
  }

  removeSelectedContacts(): void {
      this.selectedContacts = this.contacts.filter(contact => contact.checked === true);
      this.selectedId = this.selectedContacts.map(contact => contact._id);
      this.contactService.deleteMultipleContacts(this.selectedId);
  }

  ngOnInit() {
    this.initContactsDisplay();
  }
}
