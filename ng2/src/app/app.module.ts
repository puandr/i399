import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { NewComponent } from './new/new.component';
import { SearchComponent } from './search/search.component';
import { EditComponent } from './edit/edit.component';
import { FormsModule } from "@angular/forms";
import { ContactService } from "./contact.srv";
import { HttpClientModule } from "@angular/common/http";
import { FilterPipe } from './pipe'

@NgModule({
  declarations: [
    AppComponent,
    NewComponent,
    SearchComponent,
    EditComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ContactService],
  bootstrap: [AppComponent]
})
export class AppModule { }
