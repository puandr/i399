import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditComponent } from "./edit/edit.component";
import { NewComponent } from "./new/new.component";
import { SearchComponent } from "./search/search.component";

const routes: Routes = [
    { path: 'edit/:id', component: EditComponent },
    { path: 'new', component: NewComponent },
    { path: 'search', component: SearchComponent },
    { path: '', redirectTo: 'search', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
