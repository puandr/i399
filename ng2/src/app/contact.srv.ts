import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Contact} from "./contact";


@Injectable()
export class ContactService {

    constructor(private http: HttpClient) {    }

    public getContacts(): Promise<Contact[]> {
        return this.http.get('/contacts')
            .toPromise()
            .then( result => <Contact[]> result)
    }


    public getContact(id: number): Promise<Contact> {
        return this.http.get('/contacts/' + id)
            .toPromise()
            .then(result => <Contact>result)
    }


    public addContact(contact: Contact): Promise<void> {
        return this.http.post('/contacts', contact)
            .toPromise()
            .then(()=> <void>null)
            .catch(function(e) {
                console.error("Unable to add new contact", e)
            })

    }


    public editContact(id: number, contact: Contact): Promise<void> {
        return this.http.put('/contacts/' + id, contact)
            .toPromise().then(() => <void>null)
            .catch(function(e) {
                console.error("Unable to modify contact with id: " + id, e)
            })

    }


    public deleteContact(id: number): Promise<void> {
        return this.http.delete('/contacts/' + id)
            .toPromise().then(() => <void>null)
            .catch(function (e) {
                console.error("Unable to delete contact by id: " + id, e)
            })
    }


    public deleteMultipleContacts(contacts: number[]){
        return contacts.map(id => this.deleteContact(id));
    }
    
}
