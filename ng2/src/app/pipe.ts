import { Pipe, PipeTransform } from '@angular/core';

//Võetud https://stackoverflow.com/questions/44769748/angular-4-pipe-filter
//ja https://plnkr.co/edit/vRvnNUULmBpkbLUYk4uw?p=preview

@Pipe({name: 'filter', pure: false })

export class FilterPipe implements PipeTransform {
    transform(contacts: any[], query: string): any {
        return query
            ? contacts.filter(contact => contact.name.indexOf(query) !== -1 || contact.phone.indexOf(query) !== -1)
            : contacts;
    }
}