import { Component, OnInit } from '@angular/core';
import {Contact} from "../contact";
import {ContactService} from "../contact.srv";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new',
  templateUrl: `new.html`,
  styles: []
})
export class NewComponent implements OnInit {

  constructor(private contactService: ContactService, private router: Router) { }

  public contactName: string =  '';
  public contactPhone: string = '';

    addNew() {
        this.contactService.addContact(new Contact(this.contactName,this.contactPhone))
            .then(()=> this.router.navigateByUrl('/'));
    }

  ngOnInit() {
  }

}
